#pragma checksum "C:\Users\lilre\Documents\Software engineering project\gradexam\GradExam\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d04372a4919619ab0476dcde30f258612ed59ed6"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Index.cshtml", typeof(AspNetCore.Views_Home_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\lilre\Documents\Software engineering project\gradexam\GradExam\Views\_ViewImports.cshtml"
using GradExam;

#line default
#line hidden
#line 2 "C:\Users\lilre\Documents\Software engineering project\gradexam\GradExam\Views\_ViewImports.cshtml"
using GradExam.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d04372a4919619ab0476dcde30f258612ed59ed6", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"cc409dbbe0abc96de5a3131d9f014706b0bfe841", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<GradExam.ViewModels.HomeIndexViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\Users\lilre\Documents\Software engineering project\gradexam\GradExam\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
            BeginContext(92, 562, true);
            WriteLiteral(@"

<div class=""col-sm-6"">
    <h2>Index</h2>
    <p>This is a test for Nikole</p>
    <p>This is a test for the whole team.</p>

    <div>
        <a href=""/Home/Admin"">Admin Works</a>
    </div>
    <div>
        <a href=""/Home/Director"">Director Works</a>
    </div>
    <div>
        <a href=""/Home/Instructor"">Instructor Works</a>
    </div>
    <div>
        <a href=""/Home/Basic"">Basic Works</a>
    </div>
</div>

<div class=""col-sm-6"">
    <h3>List of Users to Log In</h3>
    <p>Password for all is: <b>password</b></p>
    <ul>
");
            EndContext();
#line 30 "C:\Users\lilre\Documents\Software engineering project\gradexam\GradExam\Views\Home\Index.cshtml"
         foreach(var user in Model.Users)
        {

#line default
#line hidden
            BeginContext(708, 20, true);
            WriteLiteral("                <li>");
            EndContext();
            BeginContext(729, 13, false);
#line 32 "C:\Users\lilre\Documents\Software engineering project\gradexam\GradExam\Views\Home\Index.cshtml"
               Write(user.UserName);

#line default
#line hidden
            EndContext();
            BeginContext(742, 7, true);
            WriteLiteral("</li>\r\n");
            EndContext();
#line 33 "C:\Users\lilre\Documents\Software engineering project\gradexam\GradExam\Views\Home\Index.cshtml"
        }

#line default
#line hidden
            BeginContext(760, 23, true);
            WriteLiteral("    </ul>\r\n</div>\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<GradExam.ViewModels.HomeIndexViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
